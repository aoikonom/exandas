//
//  AppDelegate.h
//  kosexandas
//
//  Created by Konstantinos Pachatouridis on 25/10/2017.
//  Copyright © 2017 Link-Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

