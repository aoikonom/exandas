//
//  ViewController.m
//  kosexandas
//
//  Created by Konstantinos Pachatouridis on 25/10/2017.
//  Copyright © 2017 Link-Technologies. All rights reserved.
//

#import "ViewController.h"
@import WebKit;

@interface ViewController ()
@end

@implementation ViewController

WKWebView *webView;

-(void)loadView {
    [super loadView];
    
    webView = [[WKWebView alloc] initWithFrame:self.view.bounds];
      //Change self.view.bounds to a smaller CGRect if you don't want it to take up the whole screen
     
    [self.view addSubview:webView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

   
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://exandas-gis.com/mobile"]]];
}

@end
